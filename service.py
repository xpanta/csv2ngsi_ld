__author__ = 'Chris Pantazis'
from ftplib import FTP
from dateutil.parser import parse, ParserError
import csv
import time
import requests
import logging
import json
from os import path, makedirs, umask, listdir, getcwd
from lib.payloads import post_device, patch_observed
from lib.db import get_value_from_static
from lib.connect import locations, url, headers, columns, exists_url, patch_url, datetime_format
from lib.common import get_hash_memory_optimized as get_digest


if path.isfile("lib/secret.py"):
    from lib.secret import sensitive_data as local_secret
    sensitive_data = local_secret

def extract_most_recent(data, loc):
    _dict = {}  # { unique_id -> (datetime, value, flag) }
    i = columns[loc]["unique_id"]
    j = columns[loc]["datetime"]
    for row in data:
        symbol = row[i]
        datetime = row[j]
        datetime = parse(datetime)
        try:
            value = float(row[columns[loc]["value"]].strip())
        except ValueError:
            continue  # if value is illegal proceed to next row
        try:
            flag = float(row[columns[loc]["quality"]].strip())
        except ValueError:
            flag = 0  # if flag is illegal it's OK (might be better to have a default flag value for illegal flag values)

        _tuple = _dict.get(symbol, ())
        if not _tuple:
            _tuple = (datetime, value, flag)
        else:
            if datetime > _tuple[0]:
                _tuple = (datetime, value, flag)
        _dict[symbol] = _tuple

    return _dict


def download_most_recent(location):
    _path = "downloads/"
    rows = []
    ftp_list = []
    connection = FTP(location["url"])
    connection.login(location["username"], location["password"])
    connection.cwd(location["path"])
    connection.dir("-t", ftp_list.append)
    if ftp_list:
        ftp_list = ftp_list[:1]   # most recent file
        line = ftp_list[0].split()
        name = line[8]
        local_file = open(path.join(_path, name), 'wb')
        connection.set_pasv(True)
        connection.retrbinary("RETR " + name, local_file.write, 1024)
        digest = get_digest(path.join(_path, name))
        local_file.close()
        with open(path.join(_path, name), 'r') as f:
            data = csv.reader(f, lineterminator='\r\n',
                              delimiter=location["delimiter"],
                              skipinitialspace=True, quotechar="\"", )
            for row in data:
                rows.append(row)
    return rows


def send_to_orion(data, post_pld, patch_pld):
    keys = data.keys()
    for key in keys:
        try:
            symbol = key
            if symbol.startswith("0000"):
                symbol = symbol[4:]
            datetime = data[key][0].strftime(datetime_format)
            value = data[key][1]
            flag = data[key][2]
            _id = "urn:ngsi-ld:Device:{}".format(symbol)
            # check if entity exists first, then patch
            _url = exists_url.format(id=_id)
            check = requests.request("GET", _url)

            if check.status_code == 200:  # entity is there so patch it with new observed data
                patch_pld["controlledProperty"]["value"] = [get_value_from_static(symbol, "Property Monitored")]
                patch_pld["value"]["value"] = value
                patch_pld["dateObserved"]["value"] = datetime
                patch_pld["configuration"]["value"] = [{"parameter": "Quality",
                                                        "value": flag}]
                # print(patch_pld)
                response = requests.request("PATCH", patch_url.format(id=_id), headers=headers,
                                            data=json.dumps(patch_pld))
            else:  # entity is not there, create it!
                post_pld["id"] = "urn:ngsi-ld:Device:{}".format(symbol)
                post_pld["description"]["value"] = get_value_from_static(symbol, "TITLE")
                post_pld["serialNumber"]["value"] = symbol
                post_pld["dateObserved"]["value"] = datetime
                post_pld["controlledProperty"]["value"] = [get_value_from_static(symbol, "Property Monitored")]
                post_pld["value"]["value"] = value
                post_pld["configuration"]["value"] = [{"parameter": "Time_Step", "value": get_value_from_static(symbol, "SENSOR_TIMESTEP (min)")},
                                                      {"parameter": "Scada_Connection", "value": get_value_from_static(symbol, "SCADA CONNECTION")},
                                                      {"parameter": "Gis_Id", "value": get_value_from_static(symbol, "GIS ID")},
                                                      {"parameter": "City", "value": get_value_from_static(symbol, "CITY")},
                                                      {"parameter": "Quality", "value": flag}]
                post_pld["refDeviceModel"]["object"] = "urn:ngsi-ld:DeviceModel:{}".format(symbol)
                response = requests.request("POST", url, headers=headers,
                                            data=json.dumps(post_pld))
                print("{}:{}:{}".format(symbol, response.status_code, response.text))
        except Exception as e:
            print (repr(e))


def start(iterations: int):
    x = 0
    while True:
        x += 1
        start1 = time.time()

        for i in range(0, len(locations)):
            location = locations[i]
            location.update(local_secret[i])
            post_pld = post_device
            patch_pld = patch_observed
            data = download_most_recent(location)
            mr_data = extract_most_recent(data, i)
            send_to_orion(mr_data, post_pld, patch_pld)

        end1 = time.time()
        print("*** UPDATING TOOK {} seconds".format(end1 - start1))

        if iterations > 0:
            if x >= iterations:
                break

        time.sleep(900)


