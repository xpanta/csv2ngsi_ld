__author__ = 'Chris Pantazis'
from tinydb import TinyDB, Query
from os import path
import logging

from config import static_data_unique_id_column as uid_col

log = logging.getLogger(__name__)
dbf = TinyDB("../store.json")
db_static = TinyDB("../static.json")
_filename = "all_sensors_fiware.csv"
_path = "./"

def insert_data(_dict):
    global dbf
    dbf.insert(_dict)


def insert_static_data():
    device = Query()
    # db_static.truncate()
    _file = path.join(_path, _filename)
    headers = []
    with open(_file, 'rU', encoding='utf-8-sig') as f:
        x = 0
        for line in f:
            if x == 0:
                headers = line.split(',')
                x += 1
                continue
            data = line.split(',')
            _dict = {}
            for i in range(len(data)):
                if i == uid_col:  # rename key column to unique_id
                    _dict["unique_id"] = data[uid_col].strip()
                else:
                    _dict[headers[i]] = data[i].strip()
            try:
                db_static.upsert(_dict, device.unique_id == data[uid_col])
            except Exception as e:
                print("ERROR IN UPSERTING {}".format(repr(e)))
                continue

    print("STATIC DATA INSERTION ENDED. {} OBJECTS ADDED".format(len(db_static)))


def get_value_from_static(unique_id, parameter):
    device = Query()
    _arr = db_static.search(device.unique_id == unique_id)
    try:
        return _arr[0][parameter]
    except Exception as e:
        return None


def get_unique_ids():
    device = Query()
    _arr = []
    ids = db_static.search(device.unique_id.all([]))
    for _id in ids:
        _arr.append(_id["unique_id"])

    return _arr
