__author__ = 'Chris Pantazis'

base_url = "http://116.203.226.40:1026"

url = base_url + "/ngsi-ld/v1/entities/"

patch_url = base_url + "/ngsi-ld/v1/entities/{id}/attrs/"

exists_url = base_url + "/ngsi-ld/v1/entities/{id}/"

# 2021-06-22T00:00:00.00+03
datetime_format = "%Y-%m-%dT%H:%M:%S"

headers = {
    'Link': '<https://smartdatamodels.org/context.jsonld>; '
            'rel="http://www.w3.org/ns/json-ld#context";'
            ' type="application/ld+json"',
    'Content-Type': 'application/json'
}

sensitive_data = [
    {
        "url": "",
        "username": "",
        "password": "",
    },
    {
        "url": "",
        "username": "",
        "password": "",
    },
]

locations = [
    {
        "path": "/mornos",
        "delimiter": ",",
        "quote_char": "\"",
        "file_mask": "",
        "name": "mornos",
    },
    {
        "path": "/meteoview",
        "delimiter": ",",
        "quote_char": "\"",
        "file_mask": "",
        "name": "meteoview",
    },
]

columns = [
    {
        "datetime": 0,
        "unique_id": 1,
        "value": 3,
        "quality": 2,
    },
    {
        "datetime": 0,
        "unique_id": 1,
        "value": 3,
        "quality": 2,
    }
]