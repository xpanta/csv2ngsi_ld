__author__ = 'Chris Pantazis'
import requests
import urllib.request
import logging
import json

from lib.db import get_unique_ids, get_value_from_static
from lib.connect import base_url
# documentation: https://github.com/FIWARE/context.Orion-LD/blob/develop/doc/manuals-ld/developer-documentation.md


ld_payload = {
    "description": "Subscription for Sensor 0ANAB7",
    "type": "Subscription",
    "entities": [
        {
            "id": "urn:ngsi-ld:Device:0ANAB7",
            "type": "Device"
        }
    ],
    "watchedAttributes": [
        "value"
    ],
    "notification": {
        "attributes": [
            "dateObserved", "controlledProperty", "value"
        ],
        "endpoint": {
            "uri": "http://116.203.226.40:5050/v2/notify",
            # "uri": "http://116.203.81.148:8003/broker/notify/",
            "accept": "application/json"
        }
    },
    "expires": "2040-01-01T14:00:00.00Z"
}


v2_payload =  {
    "description": "A subscription to get info about ...",
    "subject": {
        "entities": [
            {
                "id": "",
                "type": "Measurement"
            }
        ],
        "condition": {
            "attrs": [
                ""
            ]
        }
    },
    "notification": {
        "http": {
            "url": "http://83.212.111.241:5050/v2/notify"
        },
        "attrs": [
            ""
        ]
    },
    "expires": "2040-01-01T14:00:00.00Z"
}

log = logging.getLogger(__name__)

def subscribe_devices():
    try:
        unique_ids = get_unique_ids()
        for unique_id in unique_ids:
            prop = get_value_from_static(unique_id, "Property Monitored")
            title = get_value_from_static(unique_id, "TITLE")
            symbol = unique_id
            uri = "urn:ngsi-ld:Device:{}".format(symbol)
            ld_payload["description"] = "Subscription for Sensor {}".format(symbol)
            ent = {
                "id": uri,
                "type": "Device"
            }
            ld_payload["entities"] = [ent]
            ld_payload["notification"]["attributes"] = ["dateObserved", prop, "value"]
            # POST
            body = ld_payload
            url = base_url + "/ngsi-ld/v1/subscriptions/"
            req = urllib.request.Request(url)
            req.add_header('Content-Type', 'application/json; charset=utf-8')
            req.add_header('Link', '<https://smartdatamodels.org/context.jsonld>')
            # req.add_header('Link', '<https://uri.etsi.org/ngsi-ld/v1/ngsi-ld-core-context.jsonld>')
            json_data = json.dumps(body)
            json_data_as_bytes = json_data.encode('utf-8')
            req.add_header('Content-Length', len(json_data_as_bytes))
            response = urllib.request.urlopen(req, json_data_as_bytes)
            #
            log.debug("request for subscription for {} is {}".format(
                symbol, response.status
            ))
            print (response.status, response.headers["Location"])
            #  if json body at response (r) use  this to read it
            # data = json.loads(r.read().decode(r.info().get_param('charset') or 'utf-8'))
    except Exception as e:
        print (repr(e))
        exit()

def delete_subscriptions():
    r = requests.get(base_url + "/ngsi-ld/v1/subscriptions/")
    _data = json.loads(r.content)
    i = 0
    print("found {} subscriptions".format(len(_data)))
    while len(_data) > 0:
        # For some unknown reason Orion responds only with 20 subscriptions
        for item in _data:
            _id = item["id"]
            url = base_url + "/ngsi-ld/v1/subscriptions/{}".format(_id)
            r = requests.delete(url)
            print(_id, r.status_code)

        r = requests.get(base_url + "/ngsi-ld/v1/subscriptions/")
        _data = json.loads(r.content)


def delete_entities():
    r = requests.get(base_url + "/v2/entities/")
    _data = json.loads(r.content)
    i = 0
    print("found {} entities".format(len(_data)))
    while len(_data) > 0:
        # For some unknown reason Orion responds only with 20 subscriptions
        for item in _data:
            _id = item["id"]
            url = base_url + "/v2/entities/{}".format(_id)
            r = requests.delete(url)
            print(_id, r.status_code)

        r = requests.get(base_url + "/v2/entities/")
        _data = json.loads(r.content)


def delete_ngsi_entities():
    unique_ids = get_unique_ids()
    for unique_id in unique_ids:
        symbol = unique_id
        uri = "urn:ngsi-ld:Device:{}".format(symbol)
        url = base_url + "/ngsi-ld/v1/entities/{}".format(uri)
        r = requests.delete(url)
        print(uri, r.status_code)

    for unique_id in unique_ids:
        symbol = "0000"+unique_id
        uri = "urn:ngsi-ld:Device:{}".format(symbol)
        url = base_url + "/ngsi-ld/v1/entities/{}".format(uri)
        r = requests.delete(url)
        print(uri, r.status_code)


"""
THIS DOES NOT WORK

            print(working_payload)
            json_data = json.dumps(working_payload)
            json_bytes = json_data.encode('utf-8')
            headers = {'Content-type': 'application/json; charset=utf-8',
                       'Accept': '*/*',  'Content-Length': json_bytes}
            r = requests.post("http://83.212.111.241:1026/v2/subscriptions/",
                          json=json_data, headers=headers)

"""


""" THIS WORKS

{
  "description": "Subscription for Sensor 0ANAB7",
  "subject": {
    "entities": [
      {
        "id": "urn:ngsi-ld:Device:0ANAB7",
        "type": "Measurement"
      }
    ],
    "condition": {
      "attrs": [
        "Water Level"
      ]
    }
  },
  "notification": {
    "http": {
      "url": "http://83.212.111.241:5050/v2/notify"
    },
    "attrs": ["Water level"]
  },
  "expires": "2040-01-01T14:00:00.00Z"
}
"""
