__author__ = 'Chris Pantazis'


get_device = {

}

patch_observed = {
    "controlledProperty": {
        "type": "Property",
        "value": []
    },
    "dateObserved": {
        "type": "Property",
        "value": ""
    },
    "value": {
        "type": "Property",
        "value": 0
    },
    "configuration": {
        "type": "Property",
        "value": [
            {
                "parameter": "Quality",
                "value": 0
            }
        ]
    }
}

post_device = {
    "id": "urn:ngsi-ld:Device:01",
    "type": "Device",
    "description": {
        "type": "Property",
        "value": "Downstream L9A (NTUA)"
    },
    "category": {
        "type": "Property",
        "value": ["sensor"]
    },
    "serialNumber": {
        "type": "Property",
        "value": ""
    },
    "dstAware": {
        "type": "Property",
        "value": False,
    },
    "refDeviceModel":{
        "type": "Relationship",
        "object": ""
    },
    "controlledProperty": {
        "type": "Property",
        "value": ["Battery Life"]
    },
    "value": {
        "type": "Property",
        "value": 0
    },
    "dateObserved": {
        "type": "Property",
        "value": ""
    },
    "relativePosition": {
        "type": "Property",
        "value": ""
    },
    "configuration":{
        "type": "Property",
        "value": [
            {
                "parameter": "Time_Step",
                "value": ""
            },
            {
                "parameter": "Scada_Connection",
                "value": ""
            },
            {
                "parameter": "Gis_Id",
                "value": ""
            },
            {
                "parameter": "City",
                "value": ""
            },
            {
                "parameter": "Quality",
                "value": ""
            }
        ]
    },
    "location": {
        "type": "GeoProperty",
        "value": {
            "type": "Point",
            "coordinates": [0,0]
        }
    }
}

device_model = {
    "id": "",
    "type": "DeviceModel",
    "category": {
        "type": "Property",
        "value": ["sensor"]
    },
    "function": {
        "type": "Property",
        "value": ["sensing"]
    },
    "modelName": {
        "type": "Property",
        "value": ""
    },
    "dateCreated": {
        "type": "Property",
        "value": ""
    },
    "name": {
        "type": "Property",
        "value": ""
    },
    "alternateName": {
        "type": "Property",
        "value": ""
    },
    "description": {
        "type": "Property",
        "value": ""
    },
    "dataProvider": {
        "type": "Property",
        "value": ""
    },
    "brandName": {
        "type": "Property",
        "value": ""
    },
    "manufacturerName": {
        "type": "Property",
        "value": ""
    },
    "controlledProperty": {
        "type": "Property",
        "value": []
    }
}