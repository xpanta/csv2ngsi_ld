__author__ = 'Chris Pantazis'
import hashlib

def get_hash_memory_optimized(f_path, mode='md5'):
    h = hashlib.new(mode)
    with open(f_path, 'r') as file:
        block = file.read(512)
        while block:
            h.update(block.encode('utf-8'))
            block = file.read(512)

    return h.hexdigest()
