import sys, getopt

if __name__ == '__main__':
    from service import start
    if len(sys.argv) > 1:
        arg = sys.argv[1]
        if arg == "static":
            from lib.db import insert_static_data
            insert_static_data()
        elif arg == "subscribe":
            from lib.subscribe import subscribe_devices
            subscribe_devices()
        elif arg == "delete_subs":
            from lib.subscribe import delete_subscriptions
            delete_subscriptions()
        elif arg == "delete_entities":
            from lib.subscribe import delete_ngsi_entities
            delete_ngsi_entities()

    else:
        start(0)  # 0 for infinite loops and any number for finite loops
