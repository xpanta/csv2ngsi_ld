# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

CSV2NGSI_LD is a simple application that reads files from certain FTP or local repositories, parses the files and creates NGSI_LD commands to push data into the ORION-LD Context Broker.

The only dependency is TinyDB python app, used to store sensor's static data locally (e.g. location, model, etc). Static data are read from a local csv once and used for all following NGSI-LD commands.

In order to read and store static data this script should be run with the --static directive. All possible directives can be found in main.py and are self-explanatory. If no directive is given the programme runs indefinitely and looks for changes every 15 minutes (by default).

The file "lib/secret.py" includes all the sensitive data (server url, username and password) needed from the application in order to connect and fetch data.

This code was written with "being-reusable" in mind. However, not all projects are the same and some code modifications might be necessary.

This code belongs to the public domain and can be used as is or with changes in any way the user aspires to. A mention to the author would be welcome.

Any questions feel free to contact: xpanta@mail.ntua.gr

As a final note, I recommend using this application as a systemd service.

Chris
